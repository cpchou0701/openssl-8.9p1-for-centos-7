#apt install -y gcc gcc-9 g++ g++-9 libc6-dev make autoconf libssl-dev libpcre3-dev libpam0g-dev libsystemd-dev zlib1g-dev
file="openssh-9.6p1.tar.gz"
if [ ! -f "$file" ]; then
  wget https://ftp.riken.jp/pub/OpenBSD/OpenSSH/portable/openssh-9.6p1.tar.gz
fi

tar --no-same-owner -xf $file
dir=$(basename "$file" .tar.gz)
cd $dir

mkdir -p ./systemd
/bin/cp /usr/include/systemd/* ./systemd
sed -i '131i#include "systemd/sd-daemon.h"' ./sshd.c

target_line="Accept a connection and return in a forked child"
insert_content="            /* Signal systemd that we are ready to accept connections */\n            sd_notify(0, \"READY=1\");"
line_number=$(grep -n "$target_line" ./sshd.c | cut -d ':' -f 1)
if [ -n "$line_number" ]; then
    sed -i "${line_number}i $insert_content" ./sshd.c
fi

rm -rf /usr/local/openssh
#./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/ssl  --with-pam
## export LD_LIBRARY_PATH=/usr/local/openssl/lib/
##./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/openssl  --with-pam
./configure --prefix=/usr/local/openssh --with-pam

line_num=$(grep -n '^LIBS=' Makefile | cut -d ':' -f 1)
sed -i "${line_num}s/.*/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv -lsystemd/" Makefile
#sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv -lsystemd/g' Makefile
#sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lsystemd -lcrypto -ldl -lutil -lz  -lcrypt -lresolv/g' Makefile
make && make install
cd ..
# 系統自代
# /usr/sbin/sshd
# /etc/ssh/sshd_config
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
sudo cp /etc/ssh/ssh_config /etc/ssh/ssh_config.bak


/usr/local/sbin/sshd -V
cat /usr/local/etc/sshd_config
/usr/local/sbin/bin/ssh -V
# 最後手動啟動 systemctl restart sshd
