# openssl 8.9p1 for centos 7



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/cpchou0701/openssl-8.9p1-for-centos-7.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/cpchou0701/openssl-8.9p1-for-centos-7/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.
ldconfig 是一個管理和更新 Linux 動態鏈接庫緩存的工具，用於：
查找動態庫：根據系統配置文件（如 /etc/ld.so.conf）和預設路徑（如 /lib 和 /usr/lib），查找可用的共享庫。
更新緩存：生成或更新 /etc/ld.so.cache，該文件是動態鏈接器（ld.so）用來快速定位共享庫的緩存文件。
設置符號鏈接：確保正確的共享庫符號鏈接被設置。
執行 ldconfig -p 可以顯示系統當前的共享庫緩存列表，
ldconfig -p | grep  libcrypto.so.1.1
解壓縮 OpenSSH_9.8p1, OpenSSL 1.1.1w  11 Sep 2023 至 /usr/local/openssh
tar zxf openssh.9.8p1.tar.gz -C /
ls -al /usr/local/openssh
檢查目前 sshd的路徑
systemctl status sshd
# systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2024-12-18 23:47:51 PST; 2min 53s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 3410 (sshd)
    Tasks: 4
   CGroup: /system.slice/sshd.service
           ├─3410 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
           ├─3426 sshd-session: user [priv]
           ├─3428 sshd-session: user@pts/2
           └─3429 -bash
將舊版 sshd移至 ~/sshd.old
mv /usr/sbin/sshd ~/sshd.old
更換新版 sshd		   
/bin/cp /usr/local/openssh/sbin/sshd /usr/sbin/sshd
重啟sshd服務
systemctl restart sshd
sshd組態檔
ls -al /usr/local/openssh/etc/sshd_config
