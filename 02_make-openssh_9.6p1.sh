file="openssh-9.6p1.tar.gz"
if [ ! -f "$file" ]; then
  wget https://ftp.riken.jp/pub/OpenBSD/OpenSSH/portable/openssh-9.6p1.tar.gz
fi

source ~/.bashrc
tok=$(cat /etc/passwd | grep -o "sshd:x:74:74:")
if [[ "${tok}" == "" ]]; then
  echo "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin" >> /etc/passwd
fi


tar --no-same-owner -xf $file
dir=$(basename "$file" .tar.gz)
cd $dir

mkdir -p ./systemd
/bin/cp /usr/include/systemd/* ./systemd
sed -i '131i#include "systemd/sd-daemon.h"' ./sshd.c

target_line="Accept a connection and return in a forked child"
insert_content="            /* Signal systemd that we are ready to accept connections */\n            sd_notify(0, \"READY=1\");"
line_number=$(grep -n "$target_line" ./sshd.c | cut -d ':' -f 1)
if [ -n "$line_number" ]; then
    sed -i "${line_number}i $insert_content" ./sshd.c
fi
#sed -i '2071i                /* Signal systemd that we are ready to accept connections */' ./sshd.c
#sed -i '2072i                sd_notify(0, "READY=1");' ./sshd.c
sed -i 's/OpenSSH_9.6/OpenSSH_9.6.1/g' ./version.h

rm -rf /usr/local/openssh
#./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/ssl  --with-pam
export LD_LIBRARY_PATH=/usr/local/openssl/lib/
./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/openssl  --with-pam

line_num=$(grep -n '^LIBS=' Makefile | cut -d ':' -f 1)
sed -i "${line_num}s/.*/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv -lsystemd/" Makefile
#sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv -lsystemd/g' Makefile
#sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lsystemd -lcrypto -ldl -lutil -lz  -lcrypt -lresolv/g' Makefile
make && make install
cd ..
/usr/local/openssh/sbin/sshd -V
/usr/local/openssh/bin/ssh -V
