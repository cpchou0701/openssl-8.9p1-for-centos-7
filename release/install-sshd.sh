dir=$(pwd)
cd /; tar xvf $dir/openssh-9.6p1.tar
if [[ $? == 0 ]]; then
  nohup bash $dir/background.sh 2>&1 &
else
  echo "need sudo"
fi
