tok=$(cat /etc/passwd | grep -o "sshd:x:74:74:")
if [[ "${tok}" == "" ]]; then
  echo "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin" >> /etc/passwd
fi

tar --no-same-owner -xf openssh-8.9p1.tar.gz
mkdir -p ./openssh-8.9p1/systemd
/bin/cp /usr/include/systemd/* ./openssh-8.9p1/systemd
sed -i '131i#include "systemd/sd-daemon.h"' ./openssh-8.9p1/sshd.c
sed -i '2071i                /* Signal systemd that we are ready to accept connections */' ./openssh-8.9p1/sshd.c
sed -i '2072i                sd_notify(0, "READY=1");' ./openssh-8.9p1/sshd.c
sed -i 's/OpenSSH_8.9/OpenSSH_8.9.1/g' ./openssh-8.9p1/version.h

cd openssh-8.9p1
rm -rf /usr/local/openssh
./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/ssl  --with-pam


sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv -lsystemd/g' Makefile
#sed -i 's/LIBS=-lcrypto -ldl -lutil -lz  -lcrypt -lresolv/LIBS=-lsystemd -lcrypto -ldl -lutil -lz  -lcrypt -lresolv/g' Makefile
make && make install
cd ..
/usr/local/openssh/sbin/sshd -V
/usr/local/openssh/bin/ssh -V
rm -rf ./openssh-8.9p1
