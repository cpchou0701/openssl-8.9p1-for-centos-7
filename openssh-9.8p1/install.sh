#!/bin/bash

version=$(/usr/local/openssh/sbin/sshd -V 2>&1)
expected_version="OpenSSH_9.6.1p1, OpenSSL 1.1.1w  11 Sep 2023"
expected_version2="OpenSSH_9.8p1, OpenSSL 1.1.1w  11 Sep 2023"

if [[ "$version" == *"$expected_version2"* ]]; then
    echo "目前版本已是 OpenSSH_9.81p, 無需安裝"
    exit 1
fi

if [[ ! "$version" == *"$expected_version"* ]]; then
    echo "目前版本不是 $expected_version, 無法安裝"
    exit 1
fi


##/usr/local/openssh/sbin/sshd -V
#OpenSSH_9.6.1p1, OpenSSL 1.1.1w  11 Sep 2023
##/sbin/sshd -V
#OpenSSH_9.6.1p1, OpenSSL 1.1.1w  11 Sep 2023
# 從 9.6.1p1升級至 9.8p1
vm /usr/local/openssl /usr/local/openssl-9.6p1
tar zxf openssh.9.8p1.tar.gz -C /
nohup sh background.sh 2>&1 &

