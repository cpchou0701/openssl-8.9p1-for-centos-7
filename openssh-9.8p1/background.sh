/bin/mv /etc/ssh/sshd_config /etc/ssh/sshd_config_9.6
ln /usr/local/openssh/etc/sshd_config /etc/ssh/sshd_config
#echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "UsePAM yes" >> /etc/ssh/sshd_config
/sbin/chkconfig sshd off
systemctl stop sshd
echo "" > ~/.ssh/known_hosts
/sbin/chkconfig sshd on
pkill sshd
sleep 3
/bin/cp /usr/local/openssh/sbin/sshd /usr/sbin/sshd
/bin/cp /usr/local/openssh/bin/* /usr/bin/
/bin/cp /usr/local/openssh/bin/ssh-keygen /usr/bin/ssh-keygen
/bin/cp /usr/local/openssh/bin/scp /usr/bin/scp
/bin/cp /usr/local/openssh/etc/ssh_host_ecdsa_key.pub /etc/ssh/ssh_host_ecdsa_key.pub
systemctl daemon-reload
systemctl start sshd
echo "9.8p1" > ~/install.log
