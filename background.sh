#!/bin/bash
mkdir -p /var/empty
# 定义要添加的 LD_LIBRARY_PATH 变量的完整字符串
new_ld_library_path='export LD_LIBRARY_PATH="/usr/lib:/usr/local/openssl/lib/:$LD_LIBRARY_PATH"'

# 检查 /etc/profile 文件中是否已经存在 LD_LIBRARY_PATH 变量
if grep -q "^$new_ld_library_path" /etc/profile; then
    echo "LD_LIBRARY_PATH variable already exists in /etc/profile."
else
    # 如果不存在，则将 LD_LIBRARY_PATH 变量添加到 /etc/profile 文件中
    echo "$new_ld_library_path" | sudo tee -a /etc/profile >/dev/null
    echo "LD_LIBRARY_PATH variable added to /etc/profile."
fi

mv /usr/bin/openssl /usr/bin/openssl.bak
mv /usr/include/openssl /usr/include/openssl.bak

ln -s /usr/local/openssl/bin/openssl /usr/bin/openssl
ln -s /usr/local/openssl/include/openssl /usr/include/openssl
new_ld_library_path="/usr/local/openssl/lib"
if grep -q "^$new_ld_library_path" /etc/ld.so.conf; then
    echo "LD_LIBRARY_PATH variable already exists in /etc/profile."
else
    echo "$new_ld_library_path" | sudo tee -a /etc/ld.so.conf >/dev/null
fi
#echo "/usr/local/openssl/lib" >> /etc/ld.so.conf
/bin/cp /usr/local/openssl/lib/libcrypto.so.1.1 /usr/lib

ldconfig -v


source ~/.bashrc
/bin/rm -f /etc/ssh/sshd_config
ln /usr/local/openssh/etc/sshd_config /etc/ssh/sshd_config
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "UsePAM yes" >> /etc/ssh/sshd_config
/sbin/chkconfig sshd off
systemctl stop sshd
echo "" > ~/.ssh/known_hosts
##/bin/cp /etc/init.d/sshd /etc/init.d/sshd.bak
##/bin/cp ./openssh-9.4p1/contrib/redhat/sshd.init /etc/init.d/sshd
/sbin/chkconfig sshd on
pkill sshd
sleep 3
/bin/cp /usr/local/openssh/sbin/sshd /usr/sbin/sshd
/bin/cp /usr/local/openssh/bin/* /usr/bin/
/bin/cp /usr/local/openssh/bin/ssh-keygen /usr/bin/ssh-keygen
/bin/cp /usr/local/openssh/bin/scp /usr/bin/scp
/bin/cp /usr/local/openssh/etc/ssh_host_ecdsa_key.pub /etc/ssh/ssh_host_ecdsa_key.pub
systemctl daemon-reload
systemctl start sshd
echo "9.6p1" > ~/install.log
/usr/sbin/sshd -T | grep kex >> ~/install.log

sshd -T | grep  'diffie-hellman-group-exchange-sha1\|diffie-hellman-group1-sha1\|gss-gex-sha1-*\|gss-group1-sha1-\|gss-group14-sha1-\|rsa1024-sha1' >> ~/my.log

#kexalgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256,diffie-hellman-group-exchange-sha1,diffie-hellman-group1-sha1
