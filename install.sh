tar --no-same-owner -xf zlib-1.2.11.tar.gz
#tar --no-same-owner -xf openssl-1.1.1n.tar.gz
tar --no-same-owner -xf openssh-8.9p1.tar.gz


cd openssh-8.9p1
rm -rf /usr/local/openssh
./configure --prefix=/usr/local/openssh --with-zlib=/usr/local/zlib --with-ssl-dir=/usr/local/ssl
make && make install
yum remove openssh -y
rm -f /etc/ssh/sshd_config
ln /usr/local/openssh/etc/sshd_config /etc/ssh/sshd_config
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
/bin/cp ./contrib/redhat/sshd.init /etc/init.d/sshd
#chkconfig --add sshd
systemctl enable sshd.service
/bin/cp /usr/local/openssh/sbin/sshd /usr/sbin/sshd
/bin/cp /usr/local/openssh/bin/ssh /usr/bin/ssh
/bin/cp /usr/local/openssh/bin/ssh-keygen /usr/bin/ssh-keygen
/bin/cp /usr/local/openssh/bin/scp /usr/bin/scp
/bin/cp /usr/local/openssh/etc/ssh_host_ecdsa_key.pub /etc/ssh/ssh_host_ecdsa_key.pub
