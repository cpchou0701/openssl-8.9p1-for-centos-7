file="openssl-1.1.1w.tar.gz"
if [ ! -f "$file" ]; then
  wget https://ftp.openssl.org/source/old/1.1.1/openssl-1.1.1w.tar.gz
fi

#lib_path="export LD_LIBRARY_PATH=/usr/lib:/usr/local/openssl/lib/" 
lib_path='export LD_LIBRARY_PATH="/usr/lib:/usr/local/openssl/lib/:$LD_LIBRARY_PATH"'
to_file="/etc/profile"
if ! grep -q "$lib_path" $to_file;  then
  echo "$lib_path" >> $to_file
  source $to_file 
fi
lib_path="LD_LIBRARY_PATH=/usr/local/openssl/lib/" 
to_file="/etc/sysconfig/sshd"
if [[ ! -e $to_file ]]; then
  echo "$to_file not exist."
  echo "Check /lib/systemd/system/sshd.service, "
  echo "or /etc/systemd/system/sshd.service"
else
  if ! grep -q "$lib_path" $to_file;  then
    echo "$lib_path" >> $to_file
  fi
  yum remove openssl -y
fi

mv /usr/bin/openssl /usr/bin/openssl.bak
mv /usr/include/openssl /usr/include/openssl.bak

tar -zxf  $file
dir=$(basename "$file" .tar.gz)
cd $dir
#cd openssl-1.1.1t
./config --prefix=/usr/local/openssl 
make && make install
if [[ $? -ne 0 ]]; then  
  exit;
fi
cd ..
rm -rf ./$dir
#!/bin/bash

# 定义要添加的 LD_LIBRARY_PATH 变量的完整字符串
new_ld_library_path='LD_LIBRARY_PATH="/usr/local/openssl/lib/:$LD_LIBRARY_PATH"'

# 检查 /etc/environment 文件中是否已经存在 LD_LIBRARY_PATH 变量
if grep -q "^$new_ld_library_path" /etc/environment; then
    echo "LD_LIBRARY_PATH variable already exists in /etc/environment."
else
    # 如果不存在，则将 LD_LIBRARY_PATH 变量添加到 /etc/environment 文件中
    echo "$new_ld_library_path" | sudo tee -a /etc/environment >/dev/null
    echo "LD_LIBRARY_PATH variable added to /etc/environment."
fi

export LD_LIBRARY_PATH=/usr/local/openssl/lib/

ln -s /usr/local/openssl/bin/openssl /usr/bin/openssl
ln -s /usr/local/openssl/include/openssl /usr/include/openssl
echo “/usr/local/openssl/lib” >> /etc/ld.so.conf
/bin/cp /usr/local/openssl/lib/libcrypto.so.1.1 /usr/lib
ldconfig -v
openssl version

